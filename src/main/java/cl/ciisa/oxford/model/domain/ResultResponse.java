package cl.ciisa.oxford.model.domain;

import java.util.HashMap;
import java.util.List;

public class ResultResponse {

    private Long id;
    private String palabra;
    private List<String> definiciones;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public List<String> getDefiniciones() {
        return definiciones;
    }

    public void setDefiniciones(List<String> definiciones) {
        this.definiciones = definiciones;
    }

    

}
