# API Dicccionario

## Introducción

Esta es mi presentación para la Evaluación 3 - Unidad 3 de Taller de Aplicaciones Empresariales

## Instalación

### Configuración de base de datos

Para la configuración de la base de datos debemos incorporar el siguiente extracto xml en el archivo webapp/WEB-INF/web.xml
```xml
<data-source>
    <description>Data Source</description>
    <name>java:app/datasource</name>
    <class-name>org.postgresql.ds.PGSimpleDataSource</class-name>
    <server-name>localhost</server-name>
    <port-number>5432</port-number>
    <database-name>postgres</database-name>
    <user>postgres</user>
    <password>changeme</password>
    <login-timeout>0</login-timeout>
    <transactional>false</transactional>
    <isolation-level>TRANSACTION_READ_COMMITTED</isolation-level>
    <initial-pool-size>2</initial-pool-size>
    <max-pool-size>10</max-pool-size>
    <min-pool-size>5</min-pool-size>
    <max-idle-time>10</max-idle-time>
    <max-statements>0</max-statements>
</data-source>
<data-source>
    <description>Data Source Heroku</description>
    <name>java:app/ds-heroku</name>
    <class-name>org.postgresql.ds.PGSimpleDataSource</class-name>
    <server-name>ec2-54-165-36-134.compute-1.amazonaws.com</server-name>
    <port-number>5432</port-number>
    <database-name>d1de083midqt3v</database-name>
    <user>fxjjreavelnufk</user>
    <password>df5682d1cc6e82f0c29eb607885bbe047477d814a8c64d558ad8fa309d358c52</password>
    <login-timeout>0</login-timeout>
    <transactional>false</transactional>
    <isolation-level>TRANSACTION_READ_COMMITTED</isolation-level>
    <initial-pool-size>2</initial-pool-size>
    <max-pool-size>10</max-pool-size>
    <min-pool-size>5</min-pool-size>
    <max-idle-time>10</max-idle-time>
    <max-statements>0</max-statements>
</data-source>
```

### Crear objetos de BD

Para crear la tabla de datos se debe ejecutar el siguiente comando DDL
```sql
-- public.productos definition
-- Drop table
-- DROP TABLE public.productos;
CREATE TABLE public.productos (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	nombre varchar NULL,
	complemento varchar NULL,
	unidad varchar NULL,
	stock int4 NULL DEFAULT 0
);
```
Para regenerar la tabla, debe ejecutar el comando **DROP TABLE public.productos**

### Configuración de Payara Micro

Para configurar el plugin Payara Micro (Microprofile) se debe incluir en el pom.xml el siguiente bloque xml.

```xml
<plugin>
    <groupId>fish.payara.maven.plugins</groupId>
    <artifactId>payara-micro-maven-plugin</artifactId>
    <configuration>
        <payaraVersion>${version.payara}</payaraVersion>
        <deployWar>false</deployWar>
        <commandLineOptions>
            <option>
                <key>--autoBindHttp</key>
            </option>
            <option>
                <key>--deploy</key>
                <value>${project.build.directory}/${project.build.finalName}</value>
            </option>
            <option>
                <key>--contextroot</key>
                <value>/</value>
            </option>
        </commandLineOptions>
    </configuration>
    <version>1.0.5</version>
</plugin>
```

### Configuración de persistence.xml

Se pueden configurar diferentes conexiones a base de datos, para esto en el archivo **persistence.xml** se debe seleccionar el datasource a utilizar.

Ejemplo:
```xml
  <persistence-unit name="db" transaction-type="RESOURCE_LOCAL">
    <non-jta-data-source>java:app/ds-heroku</non-jta-data-source>
    <class>cl.ciisa.productos.model.entities.Definicion</class>
  </persistence-unit>
```

### Configuración de Procfile
Para desplegar este servicio como Payara Micro, debemos configurar el siguiente archivo Procfile

```text
web:  java -jar lib/payara-micro-5.201.jar --deploy target/productos-eva03-1.0.war --port $PORT --contextroot /
```

## Compilación y Ejecución

Para probar el software localmente se debe ejecutar los siguientes comando:

* *Descargar y preparar Maven:* En la raíz del repositorio se debe ejecutar:
```bash
mvn clean install
```
* *Compilar y empaquetar:* En la raíz del repositorio se debe ejecutar:
```bash
mvn compile package
```
* *Ejecutar Payara Micro:* En la raíz del repositorio se debe ejecutar:
```bash
mvn payara-micro:start
```

